package keith.jpa.spring.rawdao;

import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import keith.domain.jpa.entity.StudentM;

/**
 * 
 * @author Keith F. Jayme
 *
 */
@Repository
public class StudentDao
{
    @PersistenceContext(unitName = "jpa-project")
    private EntityManager entityManager;

    public List<StudentM> findAll()
    {
        CriteriaQuery<StudentM> criteriaQuery = entityManager.getCriteriaBuilder().createQuery(StudentM.class);
        Root<StudentM> root = criteriaQuery.from(StudentM.class);
        criteriaQuery.select(root);
        return entityManager.createQuery(criteriaQuery).getResultList();
    }

    public List<StudentM> findAll(String jpqlQuery, Map<String, Object> params)
    {
        TypedQuery<StudentM> query = entityManager.createQuery(jpqlQuery, StudentM.class);
        for (Map.Entry<String, Object> entry : params.entrySet())
        {
            String key = entry.getKey();
            Object value = entry.getValue();
            query.setParameter(key, value);
        }
        return query.getResultList();
    }

    public StudentM findOne(Long studentId)
    {
        return entityManager.find(StudentM.class, studentId);
    }

    @Transactional
    public StudentM save(StudentM student)
    {
        StudentM savedStudent = entityManager.merge(student);
        return savedStudent;
    }

    @Transactional
    public void delete(Long studentId) throws Exception
    {
        StudentM studentToDelete = entityManager.find(StudentM.class, studentId);
        entityManager.remove(studentToDelete);
    }

    @Transactional
    public void truncateTable() throws Exception
    {
        entityManager.createNativeQuery("SET FOREIGN_KEY_CHECKS = 0").executeUpdate();
        entityManager.createNativeQuery("truncate table student_m").executeUpdate();
        entityManager.createNativeQuery("SET FOREIGN_KEY_CHECKS = 1").executeUpdate();
    }
}
