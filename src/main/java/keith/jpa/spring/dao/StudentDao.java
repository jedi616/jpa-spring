package keith.jpa.spring.dao;

import keith.domain.jpa.entity.StudentM;
import keith.jpa.spring.dao.generic.GenericDao;

/**
 * 
 * @author Keith F. Jayme
 *
 */
public interface StudentDao extends GenericDao<StudentM, Long>
{
}
