package keith.jpa.spring.dao.generic;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Table;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.springframework.transaction.annotation.Transactional;

/**
 * 
 * @author Keith F. Jayme
 *
 */
public abstract class GenericDaoImpl<E, ID extends Serializable> implements GenericDao<E, ID>
{
    @PersistenceContext(unitName = "jpa-project")
    private EntityManager entityManager;

    private Class<E> entityClass;

    @SuppressWarnings("unchecked")
    public GenericDaoImpl()
    {
        ParameterizedType genericSuperclass = (ParameterizedType) getClass().getGenericSuperclass();
        entityClass = (Class<E>) genericSuperclass.getActualTypeArguments()[0];
    }

    @Override
    public List<E> findAll() throws Exception
    {
        CriteriaQuery<E> criteriaQuery = entityManager.getCriteriaBuilder().createQuery(entityClass);
        Root<E> root = criteriaQuery.from(entityClass);
        criteriaQuery.select(root);
        return entityManager.createQuery(criteriaQuery).getResultList();
    }

    @Override
    public List<E> findAll(String jpqlQuery, Map<String, Object> params) throws Exception
    {
        TypedQuery<E> query = entityManager.createQuery(jpqlQuery, entityClass);
        for (Map.Entry<String, Object> entry : params.entrySet())
        {
            String key = entry.getKey();
            Object value = entry.getValue();
            query.setParameter(key, value);
        }
        return query.getResultList();
    }

    @Override
    public E findOne(ID id) throws Exception
    {
        return entityManager.find(entityClass, id);
    }

    @Override
    @Transactional
    public E save(E entity) throws Exception
    {
        E savedEntity = entityManager.merge(entity);
        return savedEntity;
    }

    @Override
    @Transactional
    public void delete(ID id) throws Exception
    {
        E entityToDelete = entityManager.find(entityClass, id);
        entityManager.remove(entityToDelete);
    }

    @Override
    @Transactional
    public void truncateTable() throws Exception
    {
        System.out.println("GENERIC:");

        entityManager.createNativeQuery("SET FOREIGN_KEY_CHECKS = 0").executeUpdate();

        Table tableAnnotation = entityClass.getAnnotation(Table.class);
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("truncate table ");
        stringBuilder.append(tableAnnotation.name());
        entityManager.createNativeQuery(stringBuilder.toString()).executeUpdate();

        entityManager.createNativeQuery("SET FOREIGN_KEY_CHECKS = 1").executeUpdate();
    }
}
