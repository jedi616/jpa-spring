package keith.jpa.spring.dao;

import org.springframework.stereotype.Repository;

import keith.domain.jpa.entity.StudentM;
import keith.jpa.spring.dao.generic.GenericDaoImpl;

/**
 * 
 * @author Keith F. Jayme
 *
 */
@Repository
public class StudentDaoImpl extends GenericDaoImpl<StudentM, Long> implements StudentDao
{
}
