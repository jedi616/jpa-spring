package keith.jpa.spring.rawdao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import keith.domain.jpa.entity.StudentM;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(value = "classpath:spring/application-context.xml")
public class StudentDaoTest
{
    @Autowired
    private StudentDao dao;

    @Test
    public void test() throws Exception
    {
        // Truncate Table student_m
        dao.truncateTable();
        System.out.println("student_m table truncated...");
        System.out.println();

        // Create
        StudentM newStudent = new StudentM();
        newStudent.setName("Jimmy");
        dao.save(newStudent);
        newStudent = new StudentM();
        newStudent.setName("Ana");
        dao.save(newStudent);
        newStudent = new StudentM();
        newStudent.setName("Tom");
        dao.save(newStudent);

        // Retrieve or List
        List<StudentM> students = dao.findAll();
        displayStudents(students);

        // Update
        StudentM studentToUpdate = new StudentM();
        studentToUpdate.setStudentId(3L);
        studentToUpdate.setName("John");
        dao.save(studentToUpdate);

        // Delete
        dao.delete(2L);

        // Retrieve or List
        students = dao.findAll();
        displayStudents(students);

        // Search for Student's name
        String hqlQuery = "Select s From StudentM s Where s.name = :nameParam";
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("nameParam", "Jimmy");
        students = dao.findAll(hqlQuery, params);
        displayStudents(students);
    }

    private void displayStudents(List<StudentM> students)
    {
        for (StudentM student : students)
        {
            System.out.print("id=" + student.getStudentId() + "; ");
            System.out.println("name=" + student.getName() + "; ");
        }
        System.out.println();
    }
}
